# COAT 1.0: Composition-Adjusted Thresholding Method

![image](https://gitlab.com/ZiqianZhuang/coat/blob/master/example/example.png)

## Description

STAT946 project. An advanced method to address the problem of covariance estimation for high-dimensional compositional data. It uses entry-adaptive thresholds instead of a universal threshold 
to improve the performance. You can implement coat method to estimate the sparse covariance
matrix of the latent log-basis component and further overcome the difficulties that arise from 
unit-sum constraint of observed compositional data. Comparing with other existing similar
methods, the package outperforms in both estimation and support recovery and it is computationally much faster than existing optimization-based estimator.

Package dependencies: stats, igraph

## Case

An R script to perform all the functions from the package can be found in [example](https://gitlab.com/ZiqianZhuang/coat/tree/master/example).

## Updates #######################################################################

-2019-12-23 Updated codes, one example and slides.

## Reference

COAT approach was proposed by Yuanpei Cao, Wei Lin and Hongzhe Li.

Cao, Y., Lin, W., & Li, H. (2019). Large covariance estimation for compositional data via composition-adjusted thresholding. Journal of the American Statistical Association, 114(526), 759-772.