#--------------------------------------------------------
##' Find out the range of the tuning parameter
##' @param X -----n*p data matrix
##' @return upper ----- upper bound of the tuning parameter
##'         lower ----- lower bound of the tuning parameter
##' @author Ziqian Zhuang
#---------------------------------------------------------
findrange<-function(x){
  n <- nrow(x)
  p <- ncol(x)
  cov <- cov(x)*(n-1)/n
  centered.x <- scale(x, scale = FALSE)
  theta <- (t(centered.x)^2)%*%(centered.x^2)/n - cov^2
  delta <- cov/(theta^0.5)
  delta <- abs(delta - diag(diag(delta)))
  upper <- max(delta)
  lower <- min(delta[which(delta != 0)])
  return(list(upper = upper, lower = lower))
}